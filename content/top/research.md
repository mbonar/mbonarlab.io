---
title: "Research"
omit_header_text: true
featured_image: "images/IMG_5116.jpg"
---

## Research Interests

My research integrates genomics and wildlife ecology with the goal of understanding the causes and consequences of phenotypic diversity in natural populations. Many traits of interest such as those relating to morphology, life-history, immunity, and behaivor are quantitative, where phenotypic variation is driven by both genetic and environmental variation. By investigating the genetic basis of traits, we can understand the evolutionary potential of populations, and can extend this understanding to examine the effects of genetic covariation with fitness to predict response to selection both within and across systems.

### The adaptive potential of life history traits

![caribou1](/images/research/caribou1.jpg)

The adaptive potential of a species is fundamental to evolutionary processes such as response to selection, while also holding practical relevance for conservation and management in evaluating long-term extinction risk. Quantifying the genetic variance underpinning phenotypic variation, specifically the relationships among individuals in a population and phenotypic traits, can be used to make inferences about the inheritance and evolutionary potential of those traits, without explicit knowledge of the genetic loci involved. In natural populations, calculating additive genetic variance and trait heritability can be challenging. Such metrics rely on estimating the pairwise relatedness among individuals conventionally calculated from multigenerational pedigrees derived from field observations or from parentage assignments using multi-locus genotypes. To overcome this limitation in my PhD, I used a pedigree-free quantitative genetic approach, that used genomic data to generate a genomic relatedness matrix (GRM). I then used the GRM within a mixed model approach to quantify the proportion of phenotypic variance explained by additive genetic effects (i.e., heritability) in spring migration behaviors in migrating mule deer whose migratory phenotypes I derived using GPS data. I found low heritability for broad patterns of migration timing, but higher heritability for movement decisions along the migratory route. Estimating heritability of behaviors linked to fitness can be of particular importance where variation in life-history phenology is driven by both a genetic and environmental component, and this remains a key theme in my proposed research program.

### Characterizing the genetic architecture of traits

![squirrel1](/images/research/squirrel3.jpg)

The development of genomic techniques now affords greater insight into how individual polymorphisms contribute to phenotypic variation. One way to determine the genomic regions contributing to heritable trait variation is to use genome-wide association studies (GWAS) which leverage natural genetic variation and linkage disequilibrium patterns already present in the population to map major-effect loci to a high resolution. For my PhD, I identified genes associated with a migratory direction in mule deer by undertaking pooled genome-wide scans on a population sharing the same winter range but exhibiting variation in migration direction to two distinct summer ranges. In my current postdoctoral position at Yale, I am working with Dr. Adalgisa Caccone and Dr. Bradley Consentino where we are exploring the genetic mechanisms of melanism and adaptation to urbanization in gray squirrels. By taking a GWAS approach I have identified regions of the genome associated with melanism and differentiation between urban and rural populations. In addition, I am exploring the covariation of these regions of interest by creating linkage disequilibrium networks to determine to what degree genes of interest are linked both within and across chromosomes, and using ancestral recombination graphs to test hypotheses to determine whether genetic differentiation is due to local selective sweeps or selection against gene flow.

### Maintenance of phenotypic variation

![caribou_group](/images/research/IMG_7178.jpg)

Phenotypic divergence can arise due to genetic variation, individual plasticity, or a combination of both mechanisms, and understanding why and how phenotypic variation persists remains a fundamental aim in evolutionary biology. To date I have explored the maintenance of phenotypic variation both from the phenotype perspective and the genotype perspective. In my MSc I tested hypotheses that explained the maintenance of two socio-spatial calving strategies in caribou, by correlating individual variation in calving strategy to annual reproductive success. I found support for the integration of two classical theories describing spatial patterns in nature and showed that the persistence of both calving phenotypes may represent an evolutionary stable state. Determining the genetic architecture of fitness-related traits can also help us understand why phenotypic variation persists. For my current postdoc I am integrating field-based measurements of fitness with the genomic regions I have identified that are associated with melanism to clarify the roles of selection, drift, and gene flow in maintaining clines in melanism along an urbanization gradient.

