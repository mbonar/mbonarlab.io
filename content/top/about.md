---

title: "About"

image: '/images/Bonar_fieldphoto.jpg'

omit_header_text: false

---

## About Me

{{< carousel items="1" height="500" unit="px" duration="7000" >}}

I'm a wildlife evolutionary ecologist born in Winnipeg, Manitoba, Canada. I grew up playing soccer and rugby and enjoying all that the prairies had to offer. As one of a handful in my family to pursue an undergraduate degree, and the first in my family to purse any kind of graduate education, I have often felt intimidated in academia. Through the support and guidance I received from my teachers and mentors, I realized that I have what it takes to be a leader in my STEM field. As I continue to pursue a career in academia, I aim to instill that sense of belonging and empowerment in others. I see it as my responsibility to use my privilege and platform to create space for marginalized voices in science, and continue to actively challenge the deeply rooted colonialist, elitist, sexist, and ableist narratives that dominate academic institutions.

## Current Position

**Postdoctoral Associate - Yale University** \
2023-present \
Department of Ecology and Evolutionary Biology \
New Haven, Connecticut, USA

## Education

**PhD Environmental and Life Sciences - Trent University** \
2018 – 2023 \
Department of Environmental and Life Sciences \
Peterborough, ON, Canada

**MSc Biology - Memorial University of Newfoundland** \
2015 – 2018 \
Department of Biology \
St. John’s Newfoundland, Canada

**BSc (Honours) Biology - University of Winnipeg** \
2009 – 2014 \
Department of Biology \
Winnipeg, Manitoba, Canada


## Funding & Awards

**Natural Sciences and Engineering Council (NSERC)** \
Vanier Canada Graduate Scholarship (2018 – 2022) \
Canada Graduate Scholarship -Masters (2016–2017)

**Atlantic Computational Excellence Network (ACEnet)** \
Research Fellowship (2016–2017)

**Trent University** \
Eileen Allemang Bursary (2023)
Best Oral Presentation Trent Graduate Student Symposium (2019) \
Trent University Dean’s PhD Scholarship (2018-2022)

**Memorial University of Newfoundland** \
Deans Masters Scholarship (2016–2017)

