---
title: "#ShutDownSTEM"
omit_header_text: true
featured_image: 'images/IMG_7178.jpg'
---

## #ShutDownSTEM

#ShutDownAcademia and [#ShutDownSTEM](https://science.mit.edu/shutdownstem/) is an initiative from a multi-identity, intersectional coalition of STEM professionals and academics taking action for Black lives.

My role in academia is informed by both privileges and marginalization. My privelage allowed me to feel safe and respected in many public and academic spaces as I pursue my goals in academia with fewer external barriers compared to others. As I embrace my own positions, I have learned and am still learning to recognize how various cultural, political, social, and economic factors create marginalization. I understand that without actively challenging the deeply rooted colonialist, elitist, sexist, and ableist narratives that dominate academic institutions, that I am perpetuating these systems of oppression. Below is a collection of articles, podcasts and videos I have found particularly interesting, educational, or inspiring.

#### **Read:**
* [Invisible Women](https://www.goodreads.com/book/show/41104077-invisible-women) by Caroline Criado Pérez
* [The Inconvenient Indian](https://www.penguinrandomhouse.ca/books/93028/the-inconvenient-indian-by-thomas-king/9780385664226) by Thomas King
* [So You Want to Talk About Race](https://www.knowledgebookstore.com/products/so-you-want-to-talk-about-race?_pos=1&_sid=9c18e13c8&_ss=r) by Ijeoma Oluo

#### **Watch:**
* Desmond Cole's documentary [The Skin We're In](https://www.cbc.ca/firsthand/m_episodes/the-skin-were-in)
* Dr. Tyrone Haye's [Evnin Lecture](https://mediacentral.princeton.edu/media/Evnin+Lecture+with+Tyrone+Hayes+-+February+11%2C+2020/1_0l64g0m7)
* Drew Hayden Taylor's documentary [Cottagers & Indians](https://www.cbc.ca/cbcdocspov/episodes/cottagers-indians)
* Kimberle Crenshaw's [video]( https://www.youtube.com/watch?v=yWa63FLEYsU) describing intersectionality 
* Sharon Shattuck & Ian Cheney's [Picture a Scientist](https://www.pictureascientist.com/) NOTE you can ask your library for access to the film via [ProQuest](https://go.proquest.com/watch-picture-a-scientist/).

#### **Listen:**
* Feminista Jones’ interview on the use of the term 'ally' - aired on CBCs [Out in the Open](https://www.cbc.ca/player/play/1352977475912)
* [Open Science Podcast]( https://podcasts.apple.com/us/podcast/26-intersectional-role-models-in-stem-india-johnson/id1506227206?i=1000501646396) on the importance of intersectional role models in STEM 
