---
title: "Publications"
omit_header_text: true
featured_image: 'images/IMG_4954.jpg'
---
## Publications

#### **Pre-prints**
**13.** **Bonar, M.**, Northrup, J.M., and Shafer, A.B.A. Proximate drivers of migration propensity: a meta-analysis across species. **Authorea Preprints** DOI: 10.22541/au.171355881.18519829/v1

#### **2024**
**12.** Webber, Q.M.R, Laforge, M.P., **Bonar, M.**, Vander Wal, E. (2024) The adaptive value of density-dependent habitat specialization and social network centrality. **Nature Communications** 15:4423

#### **2022**
**11.** **Bonar, M.**, Anderson, S.J., Anderson Jr., C.A., Wittemyer G., Northrup, J.M., Shafer, A.B.A. (2022) Genomic correlates for migratory direction in a free-ranging cervid. **Proceedings of the Royal Society B** 289:20221969

**10.** Northrup J, Vander Wal, E., **Bonar, M.**, Fieberg, J., Laforge, M.P., Leclerc, M., Prokopenko, C.M., Gerber, B.D. (2022) Conceptual and methodological advances in habitat selection modeling: guidelines for ecology and evolution. **Ecological Applications** 32(1):e02470

#### **2021**
**9.** Rheault, H, Anderson Jr., C.R., **Bonar, M.**, Marrotte, R.R., Ross, T., Wittemyer, G., Northrup, J.M. (2021) Some memories never fade: Inferring multi-scale memory effects on habitat selection of a migratory ungulate using step-selection functions. **Frontiers in Ecology and Evolution** 9:702818.

**8.** Huang, R.K.K, Webber, Q.M.R., Laforge, M.P., **Bonar, M.**, Robitaille A.L., Balluffi-Fry J., Zabihi-Seissan S., Vander Wal, E. (2021) Coyote diet and spatial co-occurrence with caribou. **Canadian Journal of Zoology** 99:391-399.

**7.** Laforge, M.P, **Bonar, M.**, Vander Wal, E. (2021) Tracking snowmelt to jump the green wave: Phenological drivers of migration in a northern ungulate. **Ecology** 102:e3268.

#### **2020**
**6. Bonar, M**, Lewis K.P., Webber Q.M.R., Dobbin M., Laforge M.P., Vander Wal, E. (2020) Geometry of the ideal free distribution: individual behavioural variation and annual reproductive success in aggregations of a social ungulate. **Ecology Letters** 23:1360-1369.

**5.** Webber Q.M.R, Laforge M.P., **Bonar, M.**, Robitaille A.L,. Hart C., Zabihi-Seissan S., Vander Wal, E. (2020) The ecology of individual differences empirically applied to space-use and movement tactics. **The American Naturalist** 196, E1-E15.

#### **2018 and earlier**
**4. Bonar, M**, Ellington, E.H., Lewis, K.P., Vander Wal, E. (2018). Implementing a novel movement-based approach to inferring parturition and neonate calf survival. **PLoS ONE** 13: 1-16.

**3. Bonar, M**, Laforge, M., Vander Wal, E. (2017). Observation of a p < 10−9 life history event: implications of record-late caribou parturition on ungulate reproductive ecology and field studies. **Canadian Journal of Zoology** 2:133–137

**2. Bonar, M**, Manseau, M., Geisheimer, J., Bannatyne, T. Lingle, S. (2015) The effect of terrain and female density on survival of neonatal white-tailed deer and mule deer fawns. **Ecology and Evolution** 6:4387–4402.

**1.** Kadkhodayan, R., Colette, N., **Bonar, M**, Friel, J., Giles, L.B. (2007). Prenatal Administration of an Antioxidant Cocktail Alters Postnatal Expression of Superoxide Dismutase and Catalase in Rat Lung. **Free Radical Biology and Medicine** 43:S93.

